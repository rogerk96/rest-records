const express = require("express");
const port = process.env.PORT || 5000;
var bodyParser = require('body-parser')

const db = require("./db");

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", function(req, res){
    db.query("select * from app;", null, function(error, response){
        if (error){
            res.sendStatus(500);
            res.end();
        }
        else{
            var appArray = [];
            for (var i in response.rows){
                appArray.push({"app" : response.rows[i].app_code, "desc" : response.rows[i].descript});
            }
            //res.sendStatus(200);
            res.json(appArray);
        }            
    });
})
.get("/:app_code", function(req, res){
    db.query("SELECT * FROM records WHERE app_code = '" + req.params.app_code + "' ;", null, function(error, response){
        if (error){
            res.sendStatus(500);
            res.end();
        }
        else {
            if(response.rows.length == 0){
                res.sendStatus(404);
            }else{
                var appArray = [];
                for (var i in response.rows){
                    appArray.push({"player" : response.rows[i].player, "score" : response.rows[i].score, "date" : response.rows[i].datetime});
                }
                //res.sendStatus(200);
                res.json(appArray);
            }
        }       
    })
})
.post("/:app_code", function(req, res){
    if (req.body.hasOwnProperty("player") && req.body.hasOwnProperty("score")){
        db.query("INSERT INTO records(app_code, player, score) VALUES ('" + req.params.app_code + "', '" + req.body.player + "', " + req.body.score + ")", null, function(error, response){
            if (error){
                res.sendStatus(500);
                res.end();
            }
            else {
                res.sendStatus(200);
            }
        }) 
    }else{
        res.sendStatus(422);
        res.end();
    }
})
.listen(port, function(){
    console.log("On air at "+port);
});